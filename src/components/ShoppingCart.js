import React, { Component } from "react";

export default class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [
        {
          id: 1,
          name: "Book 1",
          price: 8000,
          quantity: 0,
        },
        {
          id: 2,
          name: "Book 2",
          price: 8000,
          quantity: 0,
        },
        {
          id: 3,
          name: "Book 3",
          price: 8000,
          quantity: 0,
        },
      ],
    };
  }
  //   componentDidMount = () => {
  //     console.log(this.state.products);
  //   };
  onClickIncrement = (product) => {
    const allProducts = [...this.state.products];
    const index = allProducts.indexOf(product);
    allProducts[index].quantity++;
    this.setState({ products: allProducts });
    console.log(product);
  };
  onClickDecrement = (product) => {
    const allProducts = [...this.state.products];
    const index = allProducts.indexOf(product);
    if (allProducts[index].quantity > 0) {
      allProducts[index].quantity--;
      this.setState({ products: allProducts });
    }

    console.log(product);
  };
  render() {
    return (
      <div>
        <div>
          <h1>Shopping cart</h1>
        </div>
        <div>
          {this.state.products.map((product) => {
            return (
              <div key={product.id}>
                <ul>
                  <li>{product.name}</li>
                  <li>{product.price}</li>
                  <li>{product.quantity}</li>
                  <button
                    onClick={() => {
                      this.onClickIncrement(product);
                    }}
                  >
                    +
                  </button>
                  <button
                    onClick={() => {
                      this.onClickDecrement(product);
                    }}
                  >
                    -
                  </button>
                </ul>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
