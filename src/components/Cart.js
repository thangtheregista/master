import React from "react";

function Cart(props) {
  const { cartItems } = props;
  return (
    <div>
      <h1>this is a cart</h1>
      {cartItems !== [] ? (
        cartItems.map((item) => {
          <div>{item.title}</div>;
        })
      ) : (
        <div>nothing in cart</div>
      )}
    </div>
  );
}

export default Cart;
