import React from "react";
import Product from "./Product";

function SearchForBooks(props) {
  const { data, textbounce, addToCart } = props;

  return (
    <div>
      <input
        type="text"
        onChange={(event) => {
          textbounce(event.target.value);
        }}
        placeholder="For example: Harry Potter"
      />
      {data !== undefined ? (
        data.map((item) => {
          return (
            <Product
              id={item.id}
              key={item.etag}
              title={item.volumeInfo.title}
              addToCart={addToCart}
            />
          );
        })
      ) : (
        <div>No results.</div>
      )}
    </div>
  );
}

export default SearchForBooks;
