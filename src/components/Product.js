import React from "react";

function Product(props) {
  const { id, title, addToCart } = props;
  return (
    <div>
      <h1>hello</h1>
      <h2>{title}</h2>
      <button
        onClick={() => {
          addToCart(id, title, 9000, 1);
        }}
      >
        Add to cart
      </button>
    </div>
  );
}

export default Product;
