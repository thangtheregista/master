import "./App.css";
import React, { useState } from "react";
import Axios from "axios";
import { debounce } from "debounce";
import ShoppingCart from "./components/ShoppingCart";
import SearchForBooks from "./components/SearchForBooks";
import Cart from "./components/Cart";

class App extends React.Component {
  state = {
    data: undefined,
    cartItems: [],
  };
  textbounce = debounce((text) => {
    // this.setLoadingToTrue();
    if (text.length > 2) {
      const url = `https://www.googleapis.com/books/v1/volumes?q=${text}&printType=books&key=AIzaSyAhc3JhlDf2V0nBdBshBQuSXBBwHQUB6e0`;
      Axios.get(url)
        .then((response) => {
          const data = response.data.items;
          console.log(data);
          if (data.length !== 0) {
            this.setState({
              data: data,
              // loading: false,
            });
            // console.log(this.state.data);
          } else {
            this.setLoadingToFalse();
            console.log("No data available");
          }
        })
        .catch((errors) => {
          const error = errors.toString();
          console.log(error);
          // this.setState({
          //   loading: false,
          //   error: error,
          // });
        });
    } else {
      // this.setLoadingToFalse();
    }

    // console.log(text);
  }, 1000);

  addToCart = (id, name, price, quantity) => {
    const product = { id, name, price, quantity };
    this.setState({
      cartItems: this.state.cartItems.concat(product),
    });
    console.log(this.state.cartItems);
  };

  componentDidMount = () => {
    const url =
      "https://www.googleapis.com/books/v1/volumes?q=hobbit&printType=books&key=AIzaSyAhc3JhlDf2V0nBdBshBQuSXBBwHQUB6e0";
    Axios.get(url).then((response) => {
      const data = response.data;
      // console.log(data.items[3]);
      this.setState({ data: data.items });
      console.log(this.state.data);
    });
  };

  render() {
    return (
      <div className="App">
        {/* <ShoppingCart /> */}
        <SearchForBooks
          data={this.state.data}
          textbounce={this.textbounce}
          addToCart={this.addToCart}
        />
        <Cart cartItems={this.state.cartItems} />
      </div>
    );
  }
}

export default App;
